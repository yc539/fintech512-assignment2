var ready = (callback) => {
  if (document.readyState != "loading"){
    callback();}
  else{
    document.addEventListener("DOMContentLoaded", callback);}
}

ready(() => {
  document.getElementById('submit').addEventListener('click', handle);
  document.getElementById('stock-select').addEventListener('change', makeplot);
});

function handle(e){
  console.log('event')
    bootbox.confirm({
    title: "Final confirmation",
    message: "Do you want to submit the page?",
    buttons:{
        confirm:{
            label:'Yes',
            className: 'btn-success'
        },
        cancel:{
            label:'No',
            className:'btn-danger'
        }
    },
    callback: function (result) {
        if(result==false){
            bootbox.alert({
                message: 'Action Cancelled',
                size: 'small'
            });
        }
        else{
            bootbox.alert({
                message: 'Form Submitted',
                size: 'small'
            });
        }
      }
    })
};

function makeplot() {
  var stock = document.getElementById("stock-select");
  var stock_value = stock.value;
  console.log('stock selected');

  console.log("start")
  fetch(stock_value + '.csv')
  .then((response) => response.text())
  .catch(error => {
      alert(error)
       })
  .then((text) => {
    console.log("csv: start")
    csv().fromString(text).then(processData) 
    console.log("csv: end")
  })
  console.log("end")
};

function processData(data) {
  console.log("start")
  let x = [], y = []
  for (let i=0; i<data.length; i++) {
    row = data[i];
    x.push( row['Date'] );
    y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("end")
}

function makePlotly( x, y ){
// var stock = document.getElementById("stock-select");

console.log("start")
var traces = [{
      x: x,
      y: y
}];

var layout  = {title: $("#stock-select option:selected").text()+" Stock Price History"}

myDiv = document.getElementById('myDiv');
Plotly.newPlot(myDiv, traces, layout);
console.log("end")
};
